#include "fightmanager.h"
#include "commandswrapper.h"
#include "gamestate.h"
#include "botsettings.h"

#include <cmath>

#include <QDebug>


FightManager::FightManager(CommandsWrapper & wrapper, const BotSettings & settings, QObject *parent)
    : ActionManager(parent)
    , m_commandsWrapper(wrapper)
    , m_botSettings(settings)
    , m_isBeforeFights(true)
{
    connect(&m_commandsWrapper, &CommandsWrapper::petLetOut, this, &FightManager::selectEnemy);
    connect(&m_commandsWrapper, &CommandsWrapper::enemyInfoLoaded, this, &FightManager::fightWithEnemy);
    connect(&m_commandsWrapper, &CommandsWrapper::fightResultLoaded, this, &FightManager::afterFight);
    connect(&m_commandsWrapper, &CommandsWrapper::potionCount, this, &FightManager::drinkPotion);
    connect(&m_commandsWrapper, &CommandsWrapper::potionResult, this, &FightManager::letOutPet);
}

void FightManager::start()
{
    m_isBeforeFights = true;
    if (m_commandsWrapper.hero().health() < m_botSettings.m_minHPToFight)
        getPotionCount();
    else
        letOutPet();
}

bool FightManager::canStart() const
{
    return (m_commandsWrapper.hero().health() >= m_botSettings.m_minHPToFight || canDrinkPotion()) &&
            m_commandsWrapper.hero().silverAmount() >= m_commandsWrapper.hero().level() &&
            m_botSettings.m_needFights &&
            m_commandsWrapper.gameState().fightsCount() > m_botSettings.m_playFightsTo &&
            ((m_isBeforeFights && m_commandsWrapper.gameState().fightsCount() >= m_botSettings.m_saveFightsTo)||
             !m_isBeforeFights);
}

void FightManager::getPotionCount()
{
    if ( m_commandsWrapper.gameState().m_potionCount.isNull())
        m_commandsWrapper.getPotionCount();
    else
        drinkPotion();
}

void FightManager::drinkPotion()
{
    if (!canStart())
    {
        finish();
        return;
    }

    m_commandsWrapper.drinkPotion();
    emit info("Использование Рубиновой настойки");
}

void FightManager::letOutPet()
{
    if (!canStart())
    {
        finish();
        return;
    }

    if (m_botSettings.m_needPetLetOut && m_commandsWrapper.hero().pet().state() == Pet::inactive)
    {
        m_commandsWrapper.letOutPet();
        emit info("Выпуск зверя из клетки");
        return;
    }

    selectEnemy();
}

void FightManager::selectEnemy()
{
    if (!canStart())
    {
        finish();
        return;
    }

    BotSettings::EnemyType enemyType = m_botSettings.m_enemyType;
    const int level = m_commandsWrapper.hero().level();
    if (level == 1 && (enemyType == BotSettings::lower2 || enemyType == BotSettings::lower1))
        enemyType = BotSettings::equal;
    else if (level == 2 && enemyType == BotSettings::lower2)
        enemyType = BotSettings::equal;

    m_commandsWrapper.selectEnemy(enemyType);
    emit info("Выбор соперника");
}

void FightManager::fightWithEnemy()
{
    if (!canStart())
    {
        finish();
        return;
    }

    if ((m_botSettings.m_skipFriends && m_commandsWrapper.gameState().m_isFriend)||
            (m_botSettings.m_skipMarked && m_commandsWrapper.gameState().m_isMarked))
    {
        selectEnemy();
        return;
    }

    if (m_botSettings.m_needCheckParams)
    {
        auto enemyFeatures = m_commandsWrapper.gameState().enemyFightFeatures();
        for (auto it = enemyFeatures.cbegin(); it != enemyFeatures.cend(); ++it)
        {
            const auto& minMax = m_botSettings.m_checkParams[it.key()];
            if (it.value() < minMax.first || it.value() > minMax.second)
            {
                selectEnemy();
                return;
            }
        }
    }

    m_commandsWrapper.fightWithThisEnemy();
    emit info("Бой");
}

void FightManager::afterFight()
{
    m_isBeforeFights = false;
    letOutPet();
}

void FightManager::finish()
{
    m_isBeforeFights = true;
    emit finnished();
}

bool FightManager::canDrinkPotion() const
{
    if (!m_botSettings.m_needDrinkRedSquare)
        return false;

    const QVariant lastPotion = m_commandsWrapper.gameState().m_lastPotionDrink;
    const QVariant potionaCount = m_commandsWrapper.gameState().m_potionCount;

    const QTime currentTime = m_commandsWrapper.gameState().gameDate().time();

    return (potionaCount.isNull() || potionaCount.toInt() > 0) &&
            (lastPotion.isNull() || abs(lastPotion.toTime().secsTo(currentTime)) >= 1*60*60);
}


