#ifndef COMMANDSWRAPPER_H
#define COMMANDSWRAPPER_H

#include "hero.h"
#include "botsettings.h"

#include <QObject>
#include <functional>


class QWebEngineView;
class QStateMachine;
class QState;
class QFinalState;

class GameState;

class CommandsWrapper : public QObject
{
    Q_OBJECT

public:
    CommandsWrapper(QWebEngineView & webView, GameState & gameState, Hero & hero, QObject *parent = nullptr);

    bool isTransactionFinished() const;
    void goToPatrol();

    void stop();

    void fillHeroTrainInfo();
    void fillPetTrainInfo();
    void fillPatrolInfo();

    void selectEnemy(BotSettings::EnemyType enemyType);
    void fightWithThisEnemy();

    void letOutPet();
    void getPotionCount();
    void drinkPotion();

    void train(BotSettings::Entity whomToTrain, Person::FightFeatures feature);

    const GameState& gameState() const;
    const Hero& hero() const;

signals:
    void heroTrainInfoLoaded();
    void petTrainInfoLoaded();
    void patrolInfoLoaded();

    void enemyInfoLoaded();
    void fightResultLoaded();

    void petLetOut();
    void potionCount();
    void potionResult();

    void featureTrained();

    void trained();
    void refreshCompleted();
    void refreshPatrolCompleted();
    void gotToPatrolCompleted();

    void stopped();

public slots:
    void refresh();
    void refreshPatrol();

    void startTransaction();
    void finishTransaction();

private slots:
    void proceedTrainPageLoaded(Hero::FightFeatures feature);

private:

    void readStateFromHeader(void(CommandsWrapper::*signal)());
    void fillTrainCosts(const QString & html, Person & person);
    void fillPatrolInfo(const QString & html);

    QStateMachine * createStateMachine(const QVector<QState*> & states, QFinalState * finalState);
    void loadAndProceedPage(QString pageUrl, std::function<void(const QString&)> func, void(CommandsWrapper::*signal)());


private:
    QWebEngineView &m_webView;
    GameState &m_gameState;
    Hero &m_hero;

    bool m_isTransactionFinished;
};

#endif // COMMANDSWRAPPER_H
