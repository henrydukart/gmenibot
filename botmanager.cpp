#include "botmanager.h"

#include <QDebug>

int convertToMsecs(const QTime & time)
{
    return ((time.hour()*60 + time.minute())*60 + time.second())*1000;
}



BotManager::BotManager(QWebEngineView *webView, QObject *parent)
    : QObject(parent)
    , m_commandsWrapper(*webView, m_gameState, m_hero)
    , m_fillInfoManager(m_commandsWrapper)
    , m_fightManager(m_commandsWrapper, m_settings)
    , m_trainManager(m_commandsWrapper, m_settings)
    , m_state(stoppedState)
{
    connect(&m_commandsWrapper, &CommandsWrapper::refreshCompleted, this, &BotManager::manage);
    connect(&m_commandsWrapper, &CommandsWrapper::refreshPatrolCompleted, this, &BotManager::manage);
    connect(&m_commandsWrapper, &CommandsWrapper::gotToPatrolCompleted, this, &BotManager::manage);

    connect(&m_updateTimer, &QTimer::timeout, this, &BotManager::updateTimerTick);

    auto stateToIdle = [this](){m_state = idleState; manage();};
    connect(&m_fillInfoManager, &FillInfoManager::finnished, this, stateToIdle);
    connect(&m_fillInfoManager, &FillInfoManager::info, this, &BotManager::infoChanged);
    connect(&m_fightManager, &FightManager::finnished, this, stateToIdle);
    connect(&m_fightManager, &FightManager::info, this, &BotManager::infoChanged);
    connect(&m_trainManager, &TrainManager::finnished, this, stateToIdle);
    connect(&m_trainManager, &TrainManager::info, this, &BotManager::infoChanged);
}

void BotManager::init()
{
    m_state = inActionState;
    m_fillInfoManager.start();
}

void BotManager::start()
{
    m_state = idleState;
    m_updateTimer.start(convertToMsecs(QTime(0, 1, 0)));
    init();
}

void BotManager::stop()
{
    m_state = stoppedState;
    m_updateTimer.stop();
    m_commandsWrapper.stop();
    emit infoChanged("Остановлен");
}

bool BotManager::timerCommandWrapperProxy(wrapper_slot_t slot, QString message)
{
    if (m_state != idleState)
        return false;

    if (!m_commandsWrapper.isTransactionFinished())
    {
        QTimer::singleShot(convertToMsecs(QTime(0, 0, 10)), this,
                           [this, slot, message](){timerCommandWrapperProxy(slot, message);});
        return false;
    }

    if (!message.isEmpty())
        emit infoChanged(message);
    (m_commandsWrapper.*slot)();

    return true;
}

void BotManager::manage()
{
    if (m_state != idleState)
        return;

    if (!m_actionQueue.empty())
    {
        ActionManager * manager = m_actionQueue.dequeue();
        Q_ASSERT(manager->canStart());
        m_state = inActionState;
        manager->start();
        return;
    }

    if (m_trainManager.canStart())
    {
        m_state = inActionState;
        m_trainManager.start();
        return;
    }

    if (m_gameState.isInPatrol())
    {
        emit infoChanged("Ожидание окончания похода");
        QTimer::singleShot(convertToMsecs(m_gameState.patrolTime().addSecs(10)),
                           this, [this](){timerCommandWrapperProxy(&CommandsWrapper::refreshPatrol, "Поход окончен");});
        return;
    }

    if (m_fightManager.canStart())
    {
        m_state = inActionState;
        m_fightManager.start();
        return;
    }

    if (m_gameState.maxPatrolTime() >= 10)
    {
        emit infoChanged("В поход");
        m_commandsWrapper.goToPatrol();
        return;
    }

    if (m_hero.health() < 80)
    {
        emit infoChanged("Ожидание восстановления здоровья");
        QTimer::singleShot(convertToMsecs(QTime(0, 5)),
                           this, [this](){timerCommandWrapperProxy(&CommandsWrapper::refresh);});
        return;
    }

    emit infoChanged("Ожидание боя");
    QTimer::singleShot(convertToMsecs(m_gameState.nextFightTime()),
                       this, [this](){timerCommandWrapperProxy(&CommandsWrapper::refresh);});
}

void BotManager::updateTimerTick()
{
    QTime currentTime = QTime::currentTime();
    if (currentTime.minute() == 15)
    {
        m_actionQueue.enqueue(&m_fillInfoManager);
        manage();
    }
}

bool BotManager::isStoped() const
{
    return m_state == stoppedState;
}

BotSettings & BotManager::settings()
{
    return m_settings;
}
