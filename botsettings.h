#ifndef BOTSETTINGS_H
#define BOTSETTINGS_H

#include "hero.h"

#include <QMap>
#include <QTime>

struct BotSettings
{
    enum EnemyType
    {
        lower2,
        lower1,
        equal,
        higher1,
        higher2,
        silver,
        crystal,
        golden
    };

    enum Entity
    {
        hero,
        pet
    };

    bool m_needFights;
    EnemyType m_enemyType;

    bool m_needCheckParams;
    QMap<Person::FightFeatures, QPair<int, int>> m_checkParams;

    int m_saveFightsTo;
    int m_playFightsTo;

    int m_minHPToFight;
    bool m_needDrinkRedSquare;

    bool m_skipFriends;
    bool m_skipMarked;

    bool m_needTrainHero;
    QMap<Person::FightFeatures, int> m_heroTrainParams;
    bool m_needTrainPet;
    QMap<Person::FightFeatures, int> m_petTrainParams;

    bool m_needPetLetOut;
    bool m_needPatrol;

    bool m_needSchedule;
    QTime m_startTime;
    QTime m_stopTime;
};

#endif // BOTSETTINGS_H
