#ifndef FILLINFOMANAGER_H
#define FILLINFOMANAGER_H

#include "actionmanager.h"

class CommandsWrapper;
struct BotSettings;

class FillInfoManager : public ActionManager
{
    Q_OBJECT
public:
    FillInfoManager(CommandsWrapper & wrapper, QObject *parent = nullptr);
    virtual void start() override;
    virtual bool canStart() const override;

private slots:
    void heroTrainInfoLoaded();
    void petTrainInfoLoaded();

private:
    CommandsWrapper & m_commandsWrapper;
};

#endif // FILLINFOMANAGER_H
