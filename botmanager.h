#ifndef BOTMANAGER_H
#define BOTMANAGER_H

#include "commandswrapper.h"
#include "gamestate.h"
#include "hero.h"
#include "fillinfomanager.h"
#include "fightmanager.h"
#include "trainmanager.h"
#include "botsettings.h"

#include <QTimer>
#include <QMap>
#include <QQueue>

class QWebEngineView;

class BotManager : public QObject
{
    Q_OBJECT

    using wrapper_slot_t = void(CommandsWrapper::*)();

    enum BotManagerState
    {
        idleState,
        stoppedState,
        inActionState
    };

public:
    explicit BotManager(QWebEngineView * webView, QObject *parent = nullptr);

    BotSettings & settings();

    bool isStoped() const;

signals:
    void infoChanged(QString);

public slots:
    void start();
    void stop();

private slots:
    bool timerCommandWrapperProxy(wrapper_slot_t slot, QString message = "");
    void init();
    void manage();
    void updateTimerTick();

private:
    CommandsWrapper m_commandsWrapper;
    GameState m_gameState;
    Hero m_hero;
    BotSettings m_settings;

    FillInfoManager m_fillInfoManager;
    FightManager m_fightManager;
    TrainManager m_trainManager;

    QTimer m_updateTimer;

    BotManagerState m_state;

    QQueue<ActionManager*> m_actionQueue;
};

#endif // BOTMANAGER_H
