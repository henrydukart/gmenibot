#include "gamestate.h"

GameState::GameState()
{

}

int GameState::fightsCount() const
{
    return m_fightsCount;
}

void GameState::setFightsCount(int fightsCount)
{
    m_fightsCount = fightsCount;
}

int GameState::maxFightsCount() const
{
    return m_maxFightsCount;
}

void GameState::setMaxFightsCount(int maxFightsCount)
{
    m_maxFightsCount = maxFightsCount;
}

QTime GameState::nextFightTime() const
{
    return m_nextFightTime;
}

void GameState::setNextFightTime(const QTime &nextFightTime)
{
    m_nextFightTime = nextFightTime;
}

bool GameState::isFullFights() const
{
    return m_fightsCount == m_maxFightsCount;
}

bool GameState::isSomethingHappened() const
{
    return m_isSomethingHappened;
}

void GameState::setIsSomethingHappened(bool isSomethingHappened)
{
    m_isSomethingHappened = isSomethingHappened;
}

bool GameState::isInPatrol() const
{
    return m_patrolTime.hour() != 0 || m_patrolTime.minute() != 0 || m_patrolTime.second() !=0;
}

int GameState::maxPatrolTime() const
{
    return m_maxPatrolTime;
}

void GameState::setMaxPatrolTime(int maxPatrolTime)
{
    m_maxPatrolTime = maxPatrolTime;
}

QTime GameState::patrolTime() const
{
    return m_patrolTime;
}

void GameState::setPatrolTime(const QTime &patrolTime)
{
    m_patrolTime = patrolTime;
}

QMap<Person::FightFeatures, int> GameState::enemyFightFeatures() const
{
    return m_enemyFightFeatures;
}

void GameState::setEnemyFightFeatures(const QMap<Person::FightFeatures, int> &enemyFightFeatures)
{
    m_enemyFightFeatures = enemyFightFeatures;
}

QDateTime GameState::gameDate() const
{
    return m_gameDate;
}

void GameState::setGameDate(const QDateTime &gameDate)
{
    m_gameDate = gameDate;
}
