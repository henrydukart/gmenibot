#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "botmanager.h"
#include "interceptorwebpage.h"

#include <QWebEngineProfile>
#include <QSettings>
#include <QPair>


MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_botManager = new BotManager(ui->webView, this);

    m_settingsFile = QApplication::applicationDirPath() + "/settings.ini";
    loadSettings();
    setGUIParams();
    connectGUISlots();

    QWebEngineProfile::defaultProfile()->setHttpUserAgent("Nokia5230");
    InterceptorWebPage * page = new InterceptorWebPage(QWebEngineProfile::defaultProfile(), this);
    page->load(QUrl("http://g.meni.mobi"));
    ui->webView->setPage(page);

    connect(m_botManager, &BotManager::infoChanged,
           this, [this](QString str){ ui->statusBar->showMessage(str);});

    connect(ui->startButton, SIGNAL(clicked(bool)), SLOT(startBot()));
    connect(ui->pauseButton, SIGNAL(clicked(bool)), SLOT(pauseBot()));
    connect(ui->webView, &QWebEngineView::urlChanged, this, [this](QUrl url){this->ui->urlLine->setText(url.toString());});


    m_minuteTimer.start(60*1000);
    connect(&m_minuteTimer, &QTimer::timeout, this, &MainWindow::timerTick);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::startBot()
{
    ui->startButton->setEnabled(false);
    ui->pauseButton->setEnabled(true);
    ui->webView->setEnabled(false);
    m_botManager->start();
}

void MainWindow::pauseBot()
{
    ui->startButton->setEnabled(true);
    ui->pauseButton->setEnabled(false);
    ui->webView->setEnabled(true);
    m_botManager->stop();
}

void MainWindow::timerTick()
{
    if (!m_botManager->settings().m_needSchedule)
        return;

    QTime currentTime = QTime::currentTime();
    currentTime.setHMS(currentTime.hour(), currentTime.minute(), 0);

    if (m_botManager->isStoped() && m_botManager->settings().m_startTime == currentTime)
    {
        startBot();
        return;
    }

    if (!m_botManager->isStoped() && m_botManager->settings().m_stopTime == currentTime)
    {
        pauseBot();
        return;
    }
}

void MainWindow::closeEvent(QCloseEvent * /*event*/)
{
    saveSettings();
}

void MainWindow::loadSettings()
{
    QSettings settingsLoader(m_settingsFile, QSettings::IniFormat);
    BotSettings & settings = m_botManager->settings();

    settings.m_needFights = settingsLoader.value("needFights", true).toBool();
    settings.m_enemyType = static_cast<BotSettings::EnemyType>(
                settingsLoader.value("enemyType", BotSettings::silver).toInt());
    settings.m_needCheckParams = settingsLoader.value("needCheckParams", false).toBool();

    settingsLoader.beginReadArray("checkParams");
    for (int i = Person::power; i <= Person::weight; ++i)
    {
        settingsLoader.setArrayIndex(i);
        QPair<int, int> minMax;
        minMax.first = settingsLoader.value("min", 5).toInt();
        minMax.second = settingsLoader.value("max", 100).toInt();
        settings.m_checkParams[static_cast<Person::FightFeatures>(i)] = minMax;
    }
    settingsLoader.endArray();

    settings.m_saveFightsTo = settingsLoader.value("saveFightsTo", 6).toInt();
    settings.m_playFightsTo = settingsLoader.value("playFightsTo", 0).toInt();

    settings.m_minHPToFight = settingsLoader.value("minHPToFight", 80).toInt();
    settings.m_needDrinkRedSquare = settingsLoader.value("needDrinkRedSquare", false).toBool();

    settings.m_skipFriends = settingsLoader.value("skipFriends", true).toBool();
    settings.m_skipMarked = settingsLoader.value("skipMarked", true).toBool();

    settings.m_needPetLetOut = settingsLoader.value("needPetLetOut", false).toBool();

    settings.m_needTrainHero = settingsLoader.value("needTrainHero", false).toBool();
    settingsLoader.beginReadArray("heroTrainParams");
    for (int i = Person::power; i <= Person::weight; ++i)
    {
        settingsLoader.setArrayIndex(i);
        settings.m_heroTrainParams[static_cast<Person::FightFeatures>(i)] = settingsLoader.value("val", 5).toInt();
    }
    settingsLoader.endArray();

    settings.m_needTrainPet = settingsLoader.value("needTrainPet", false).toBool();
    settingsLoader.beginReadArray("petTrainParams");
    for (int i = Person::power; i <= Person::weight; ++i)
    {
        settingsLoader.setArrayIndex(i);
        settings.m_petTrainParams[static_cast<Person::FightFeatures>(i)] = settingsLoader.value("val", 5).toInt();
    }
    settingsLoader.endArray();

    settings.m_needSchedule = settingsLoader.value("needSchedule", false).toBool();
    settings.m_startTime = settingsLoader.value("startTime", QTime(9, 0)).toTime();
    settings.m_stopTime = settingsLoader.value("stopTime", QTime(17, 0)).toTime();
}

void MainWindow::saveSettings()
{
    QSettings settingsSaver(m_settingsFile, QSettings::IniFormat);
    const BotSettings & settings = m_botManager->settings();

    settingsSaver.setValue("needFights", settings.m_needFights);
    settingsSaver.setValue("enemyType", settings.m_enemyType);
    settingsSaver.setValue("needCheckParams", settings.m_needCheckParams);

    settingsSaver.beginWriteArray("checkParams");
    for (int i = Person::power; i <= Person::weight; ++i)
    {
        settingsSaver.setArrayIndex(i);
        settingsSaver.setValue("min", settings.m_checkParams[static_cast<Person::FightFeatures>(i)].first);
        settingsSaver.setValue("max", settings.m_checkParams[static_cast<Person::FightFeatures>(i)].second);
    }
    settingsSaver.endArray();

    settingsSaver.setValue("saveFightsTo", settings.m_saveFightsTo);
    settingsSaver.setValue("playFightsTo", settings.m_playFightsTo);

    settingsSaver.setValue("minHPToFight", settings.m_minHPToFight);
    settingsSaver.setValue("needDrinkRedSquare", settings.m_needDrinkRedSquare);

    settingsSaver.setValue("skpFriends", settings.m_skipFriends);
    settingsSaver.setValue("skipMarked", settings.m_skipMarked);

    settingsSaver.setValue("needTrainHero", settings.m_needTrainHero);
    settingsSaver.beginWriteArray("heroTrainParams");
    for (int i = Person::power; i <= Person::weight; ++i)
    {
        settingsSaver.setArrayIndex(i);
        settingsSaver.setValue("val", settings.m_heroTrainParams[static_cast<Person::FightFeatures>(i)]);
    }
    settingsSaver.endArray();

    settingsSaver.setValue("needTrainPet", settings.m_needTrainPet);
    settingsSaver.beginWriteArray("petTrainParams");
    for (int i = Person::power; i <= Person::weight; ++i)
    {
        settingsSaver.setArrayIndex(i);
        settingsSaver.setValue("val", settings.m_petTrainParams[static_cast<Person::FightFeatures>(i)]);
    }
    settingsSaver.endArray();

    settingsSaver.setValue("needPetLetOut", settings.m_needPetLetOut);

    settingsSaver.setValue("needSchedule", settings.m_needSchedule);
    settingsSaver.setValue("startTime", settings.m_startTime);
    settingsSaver.setValue("stopTime", settings.m_stopTime);
}

void MainWindow::connectGUISlots()
{
    BotSettings & settings = m_botManager->settings();

    connect(ui->enemyBox, &QGroupBox::clicked, this, [&settings](bool ok){settings.m_needFights = ok;});
    connect(ui->enemyCombo, QOverload<int>::of(&QComboBox::currentIndexChanged), this,
            [&settings](int index){settings.m_enemyType = static_cast<BotSettings::EnemyType>(index);});

    connect(ui->enemyParamsBox, &QGroupBox::clicked, this, [&settings](bool ok){settings.m_needCheckParams = ok;});
    connect(ui->minPowerValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_checkParams[Person::power].first = val;});
    connect(ui->maxPowerValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_checkParams[Person::power].second = val;});
    connect(ui->minProtectionValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_checkParams[Person::protection].first = val;});
    connect(ui->maxProtectionValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_checkParams[Person::protection].second = val;});
    connect(ui->minDexterityValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_checkParams[Person::dexterity].first = val;});
    connect(ui->maxDexterityValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_checkParams[Person::dexterity].second = val;});
    connect(ui->minSkillValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_checkParams[Person::skill].first = val;});
    connect(ui->maxSkillValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_checkParams[Person::skill].second = val;});
    connect(ui->minWeightValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_checkParams[Person::weight].first = val;});
    connect(ui->maxWeightValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_checkParams[Person::weight].second = val;});

    connect(ui->saveFightsToValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_saveFightsTo = val;});
    connect(ui->playFightsToValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_playFightsTo = val;});

    connect(ui->minHPValue, QOverload<int>::of(&QSpinBox::valueChanged), this,
            [&settings](int val){settings.m_minHPToFight = val;});
    connect(ui->drinkRedSquareCheckBox, &QCheckBox::clicked, this,
            [&settings](bool ok){settings.m_needDrinkRedSquare = ok;});

    connect(ui->skipFriendsValue, &QCheckBox::clicked, this,
            [&settings](bool ok){settings.m_skipFriends = ok;});
    connect(ui->skipMarkedValue, &QCheckBox::clicked, this,
            [&settings](bool ok){settings.m_skipMarked=ok;});

    connect(ui->needPetLetOutCheck, &QCheckBox::clicked, this, [&settings](bool ok){settings.m_needPetLetOut = ok;});

    connect(ui->trainHeroBox, &QGroupBox::clicked, this, [&settings](bool ok){settings.m_needTrainHero = ok;});
    QVector<QSpinBox*> spinBoxes{ui->heroTrainPower, ui->heroTrainProtection, ui->heroTrainDexterity,
                                ui->heroTrainSkill, ui->heroTrainWeight};
    for (int i = Person::power; i <= Person::weight; ++i)
        connect(spinBoxes[i], QOverload<int>::of(&QSpinBox::valueChanged), this,
                [&settings, i](int value){settings.m_heroTrainParams[static_cast<Person::FightFeatures>(i)] = value;});

    connect(ui->trainPetBox, &QGroupBox::clicked, this, [&settings](bool ok){settings.m_needTrainPet = ok;});
    spinBoxes.clear();
    spinBoxes << ui->petTrainPower << ui->petTrainProtection << ui->petTrainDexterity <<
                                ui->petTrainSkill << ui->petTrainWeight;
    for (int i = Person::power; i <= Person::weight; ++i)
        connect(spinBoxes[i], QOverload<int>::of(&QSpinBox::valueChanged), this,
                [&settings, i](int value){settings.m_petTrainParams[static_cast<Person::FightFeatures>(i)] = value;});

    connect(ui->scheduleTimeBox, &QGroupBox::clicked, this, [&settings](bool ok){ settings.m_needSchedule = ok;});
    connect(ui->startTimeEdit, &QTimeEdit::timeChanged, this, [&settings](QTime time){settings.m_startTime = time;});
    connect(ui->stopTimeEdit, &QTimeEdit::timeChanged, this, [&settings](QTime time){settings.m_stopTime = time;});
}

void MainWindow::setGUIParams()
{
    const BotSettings & settings = m_botManager->settings();

    ui->enemyBox->setChecked(settings.m_needFights);
    ui->enemyCombo->setCurrentIndex(settings.m_enemyType);

    ui->enemyParamsBox->setChecked(settings.m_needCheckParams);
    ui->minPowerValue->setValue(settings.m_checkParams[Person::power].first);
    ui->maxPowerValue->setValue(settings.m_checkParams[Person::power].second);
    ui->minProtectionValue->setValue(settings.m_checkParams[Person::protection].first);
    ui->maxProtectionValue->setValue(settings.m_checkParams[Person::protection].second);
    ui->minDexterityValue->setValue(settings.m_checkParams[Person::dexterity].first);
    ui->maxDexterityValue->setValue(settings.m_checkParams[Person::dexterity].second);
    ui->minSkillValue->setValue(settings.m_checkParams[Person::skill].first);
    ui->maxSkillValue->setValue(settings.m_checkParams[Person::skill].second);
    ui->minWeightValue->setValue(settings.m_checkParams[Person::weight].first);
    ui->maxWeightValue->setValue(settings.m_checkParams[Person::weight].second);

    ui->saveFightsToValue->setValue(settings.m_saveFightsTo);
    ui->playFightsToValue->setValue(settings.m_playFightsTo);

    ui->minHPValue->setValue(settings.m_minHPToFight);
    ui->drinkRedSquareCheckBox->setChecked(settings.m_needDrinkRedSquare);

    ui->skipFriendsValue->setChecked(settings.m_skipFriends);
    ui->skipMarkedValue->setChecked(settings.m_skipMarked);

    ui->needPetLetOutCheck->setChecked(settings.m_needPetLetOut);

    ui->trainHeroBox->setChecked(settings.m_needTrainHero);
    QVector<QSpinBox*> spinBoxes{ui->heroTrainPower, ui->heroTrainProtection, ui->heroTrainDexterity,
                                ui->heroTrainSkill, ui->heroTrainWeight};
    for (int i = Person::power; i <= Person::weight; ++i)
        spinBoxes[i]->setValue(settings.m_heroTrainParams[static_cast<Person::FightFeatures>(i)]);

    spinBoxes.clear();
    spinBoxes << ui->petTrainPower << ui->petTrainProtection << ui->petTrainDexterity <<
                                ui->petTrainSkill << ui->petTrainWeight;
    for (int i = Person::power; i <= Person::weight; ++i)
        spinBoxes[i]->setValue(settings.m_petTrainParams[static_cast<Person::FightFeatures>(i)]);

    ui->trainPetBox->setChecked(settings.m_needTrainPet);

    ui->scheduleTimeBox->setChecked(settings.m_needSchedule);
    ui->startTimeEdit->setTime(settings.m_startTime);
    ui->stopTimeEdit->setTime(settings.m_stopTime);
}
