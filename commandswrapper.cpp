#include "commandswrapper.h"
#include "hero.h"
#include "gamestate.h"

#include <QWebEngineView>
#include <QRegularExpression>
#include <QStateMachine>
#include <QFinalState>
#include <QTimer>
#include <QDebug>


const QUrl BASE_URL("http://g.meni.mobi");

QDateTime getCurrentDate(const QString & html)
{
    const static QRegularExpression DATA_MILLISECONDS(R"**(data-milliseconds="(\d+)")**");
    const QRegularExpressionMatch match = DATA_MILLISECONDS.match(html);
    return QDateTime::fromMSecsSinceEpoch(match.captured(1).toLongLong());
}

QState * createWaitState(QState *parent = nullptr)
{
    QState *waitState = new QState(parent);
    QTimer *timer = new QTimer(waitState);
    timer->setInterval(3000 + (qrand() % 1001 - 500));
    timer->setSingleShot(true);
    QState *timing = new QState(waitState);
    timing->addTransition(timer, SIGNAL(timeout()), new QFinalState(waitState));
    waitState->setInitialState(timing);
    QObject::connect(timing, SIGNAL(entered()), timer, SLOT(start()));
    return waitState;
}

QStateMachine * CommandsWrapper::createStateMachine(const QVector<QState*> & states, QFinalState * finalState)
{
    QStateMachine * machine = new QStateMachine();
    connect(machine, &QStateMachine::started, this, &CommandsWrapper::startTransaction);
    connect(machine, &QStateMachine::finished, this, &CommandsWrapper::finishTransaction);
    connect(machine, &QStateMachine::finished, machine, &QStateMachine::deleteLater);

    QState * groupState = new QState();
    QState * waitState = nullptr;
    for (int i = 0; i < states.size(); ++i)
    {
        QState * curState = states[i];
        curState->setParent(groupState);
        if (waitState)
            waitState->addTransition(waitState, &QState::finished, curState);
        waitState = createWaitState(groupState);
        curState->addTransition(&this->m_webView, SIGNAL(loadFinished(bool)), waitState);
    }
    waitState->addTransition(waitState, &QState::finished, finalState);
    machine->addState(finalState);

    groupState->setInitialState(states.front());
    QFinalState *stoppedState = new QFinalState();
    groupState->addTransition(this, &CommandsWrapper::stopped, stoppedState);
    machine->addState(groupState);
    machine->addState(stoppedState);
    machine->setInitialState(groupState);

    return machine;
}

void CommandsWrapper::loadAndProceedPage(QString pageUrl, std::function<void (const QString&)> func, void (CommandsWrapper::*signal)())
{
    QState * beforeLoaded = new QState;
    QFinalState * afterLoaded = new QFinalState;

    QStateMachine * machine = createStateMachine(
                QVector<QState*>() << beforeLoaded , afterLoaded);

    connect(afterLoaded, &QState::entered, [this, func, signal]()
    {
        this->m_webView.page()->toHtml([this, func, signal](const QString html)
        {
            func(html);
            readStateFromHeader(signal);
        });
    });

    machine->start();

    QStringList list = pageUrl.split("?");
    QUrl url = BASE_URL;
    url.setPath(list[0]);
    if (list.size() > 1)
        url.setQuery(list[1]);
    m_webView.load(url);
}

const Hero& CommandsWrapper::hero() const
{
    return m_hero;
}

const GameState& CommandsWrapper::gameState() const
{
    return m_gameState;
}



CommandsWrapper::CommandsWrapper(QWebEngineView& webView, GameState& gameState, Hero& hero, QObject *parent)
    : QObject(parent)
    , m_webView(webView), m_gameState(gameState), m_hero(hero), m_isTransactionFinished(true)
{

}

void CommandsWrapper::refresh()
{
    QState * beforeRefresh = new QState();
    QFinalState * afterRefresh = new QFinalState();

    QStateMachine * machine = createStateMachine(QVector<QState*>() << beforeRefresh, afterRefresh);

    connect(afterRefresh, &QState::entered, [this](){readStateFromHeader(&CommandsWrapper::refreshCompleted);});

    machine->start();

    m_webView.reload();
}

void CommandsWrapper::refreshPatrol()
{
    QState * beforeRefresh = new QState();
    QFinalState * afterRefresh = new QFinalState();

    QStateMachine * machine = createStateMachine(QVector<QState*>() << beforeRefresh, afterRefresh);

    connect(afterRefresh, &QState::entered, [this]()
    {
        this->m_webView.page()->toHtml([=](const QString html)
        {
            fillPatrolInfo(html);
            readStateFromHeader(&CommandsWrapper::refreshPatrolCompleted);
        });
    });

    machine->start();

    QUrl url = BASE_URL;
    url.setPath("/game/patrol");
    m_webView.load(url);
}

void CommandsWrapper::goToPatrol()
{
    QState * beforePatrolStarted = new QState();
    QState * patrolInfoLoaded = new QState();
    QFinalState * afterPatrolStarted = new QFinalState();

    QStateMachine * machine = createStateMachine(
                QVector<QState*>() << beforePatrolStarted << patrolInfoLoaded, afterPatrolStarted);

    connect(patrolInfoLoaded, &QState::entered, [this]()
    {
        this->m_webView.page()->toHtml([this](const QString html)
        {
            const QRegularExpression re(R"**(\/game\/patrol\/start_standard\?drid=(\d+))**");
            QUrl url = BASE_URL;
            url.setPath("/game/patrol/start_standard");
            url.setQuery("drid=" + re.match(html).captured(1));
            m_webView.load(QWebEngineHttpRequest::postRequest(url, {{"time", "10"}}));
        });
    });

    connect(afterPatrolStarted, &QState::entered, [this]()
    {
        this->m_webView.page()->toHtml([this](const QString html)
        {
            fillPatrolInfo(html);
            readStateFromHeader(&CommandsWrapper::gotToPatrolCompleted);
        });
    });

    machine->start();

    QUrl url = BASE_URL;
    url.setPath("/game/patrol");
    m_webView.load(url);
}

void CommandsWrapper::stop()
{
    emit stopped();
}

void CommandsWrapper::fillHeroTrainInfo()
{
    loadAndProceedPage("/game/buildup/user",
                        [this](QString html){fillTrainCosts(html, m_hero);},
                        &CommandsWrapper::heroTrainInfoLoaded);
}

void CommandsWrapper::fillPetTrainInfo()
{
    loadAndProceedPage("/game/buildup/pet",
                        [this](QString html){fillTrainCosts(html, m_hero.pet());},
                        &CommandsWrapper::petTrainInfoLoaded);
}

void CommandsWrapper::fillPatrolInfo()
{
    loadAndProceedPage("/game/patrol",
                        [this](QString html){fillPatrolInfo(html);},
                        &CommandsWrapper::patrolInfoLoaded);
}

void CommandsWrapper::selectEnemy(BotSettings::EnemyType enemyType)
{
    QString url = "/game/battle";
    if (enemyType == BotSettings::silver)
    {
        url += "/mercenary/new?type=silver";
    }
    else if (enemyType == BotSettings::crystal)
    {
        url += "/mercenary/new?type=crystal";
    }
    else if (enemyType == BotSettings::golden)
    {
        url += "/mercenary/new?type=gold";
    }
    else
    {
        int needLevel = m_hero.level();
        switch (enemyType)
        {
        case BotSettings::lower2:
            needLevel -= 2;
            break;
        case BotSettings::lower1:
            needLevel--;
            break;
        case BotSettings::higher1:
            needLevel++;
            break;
        case BotSettings::higher2:
            needLevel+=2;
            break;
        default:
            break;
        }
        url += "/player/find?level=" + QString::number(needLevel);
    }

    auto getEnemyInfo = [this](const QString & html)
    {
        m_gameState.m_isFriend = html.contains("(твоя)") ||
                html.contains("(твой)") ||
                html.contains("/assets/default/icons/friend");

        m_gameState.m_isMarked = html.contains("/assets/default/icons/marked");

        const QRegularExpression re(R"**(<td class="tdc">(\d+))**");
        QRegularExpressionMatchIterator matchIter = re.globalMatch(html);
        QMap<Person::FightFeatures, int> features;
        for (int i = Person::power; i <= Person::weight; ++i)
        {
            matchIter.next();
            features[static_cast<Person::FightFeatures>(i)] =
                    matchIter.next().captured(1).toInt();
        }
        m_gameState.setEnemyFightFeatures(features);
    };

    loadAndProceedPage(url,
                        getEnemyInfo,
                        &CommandsWrapper::enemyInfoLoaded);
}

void CommandsWrapper::fightWithThisEnemy()
{
    this->m_webView.page()->toHtml([this](const QString html)
    {
        const QRegularExpression re(R"**(<a href="(.+?)" class="button_medium">)**");
        loadAndProceedPage(re.match(html).captured(1),
                            [](const QString&){},
                            &CommandsWrapper::fightResultLoaded);
    });
}

void CommandsWrapper::letOutPet()
{
    loadAndProceedPage("/game/pet/activate?value=t",
                        [](const QString&){},
                        &CommandsWrapper::petLetOut
    );
}

void CommandsWrapper::getPotionCount()
{
    loadAndProceedPage("/game/inventories",
                        [this](QString html)
    {
        QRegularExpression reg(R"**(id=user_red_square.*<div class="frame_item frame_banki">\s+(\d+))**",
                               QRegularExpression::DotMatchesEverythingOption);
        QRegularExpressionMatch match = reg.match(html);
        m_gameState.m_potionCount = (match.hasMatch() ? match.captured(1).toInt() : 0);
    },
                        &CommandsWrapper::potionCount);
}

void CommandsWrapper::drinkPotion()
{
    loadAndProceedPage("/game/inventories/use?id=user_red_square",
                        [this](QString html)
    {
        QRegularExpression reg(R"**(Ты сможешь использовать через (\d+):(\d+):(\d+))**");
        QRegularExpressionMatch match = reg.match(html);

        QTime currentTime = getCurrentDate(html).time();

        if (match.hasMatch())
        {
            QTime time(match.captured(1).toInt(), match.captured(2).toInt(), match.captured(3).toInt());
            currentTime = currentTime.addMSecs(-QTime(1, 0, 0).msecsSinceStartOfDay());
            m_gameState.m_lastPotionDrink = currentTime.addMSecs(time.msecsSinceStartOfDay());
        }
        else
        {
            m_gameState.m_lastPotionDrink = currentTime;
            m_gameState.m_potionCount = m_gameState.m_potionCount.toInt()-1;
        }
    },
                        &CommandsWrapper::potionResult);
}

void CommandsWrapper::train(BotSettings::Entity whomToTrain, Person::FightFeatures feature)
{
    const QString curPageUrl = m_webView.url().path();

    QVector<QState*> states;
    QState * trainPageLoaded = new QState();
    QFinalState * afterTrain = new QFinalState();
    states << trainPageLoaded;

    bool neeLoadTrainPage = false;
    if ((whomToTrain == BotSettings::pet && !curPageUrl.endsWith("pet")) ||
            (whomToTrain == BotSettings::hero && !curPageUrl.endsWith("user")))
    {
        neeLoadTrainPage = true;
        QState * beforeTrainPageLoaded = new QState();
        connect(trainPageLoaded, &QState::entered, [feature, this](){proceedTrainPageLoaded(feature);});
        states.push_front(beforeTrainPageLoaded);
    }

    QStateMachine * machine = createStateMachine(states, afterTrain);
    machine->start();

    connect(afterTrain, &QState::entered, this, [this]()
    {
        m_webView.page()->toHtml([=](const QString &html)
        {
            if (m_webView.url().path().endsWith("user"))
                fillTrainCosts(html, m_hero);
            else
                fillTrainCosts(html, m_hero.pet());
            readStateFromHeader(&CommandsWrapper::trained);
        });
    });

    if (neeLoadTrainPage)
    {
        QUrl url = BASE_URL;
        url.setPath(QString("/game/buildup/") + (whomToTrain == BotSettings::pet ? "pet" : "user"));
        m_webView.load(url);
    }
    else
    {
        proceedTrainPageLoaded(feature);
    }
}

void CommandsWrapper::proceedTrainPageLoaded(Hero::FightFeatures feature)
{
    m_webView.page()->toHtml([=](const QString &html)
    {        
        QString stringFeature = "";
        switch(feature)
        {
        case Person::power:
            stringFeature = "power";
            break;
        case Person::protection:
            stringFeature = "protection";
            break;
        case Person::dexterity:
            stringFeature = "dexterity";
            break;
        case Person::skill:
            stringFeature = "skill";
            break;
        case Person::weight:
            stringFeature = "weight";
            break;
        }

        const QRegularExpression re(R"**(\/game\/buildup\/improve_(user|pet)\?attribute=\w+&amp;drid=(\d+))**");
        QRegularExpressionMatch match = re.match(html);

        QUrl url = BASE_URL;
        url.setPath(QString("/game/buildup/improve_%1").arg(match.captured(1)));
        url.setQuery(QString("attribute=%1&drid=%2").arg(stringFeature).arg(match.captured(2)));
        m_webView.load(url);
    });
}

void CommandsWrapper::finishTransaction()
{
    Q_ASSERT(!m_isTransactionFinished);
    m_isTransactionFinished = true;
}

void CommandsWrapper::startTransaction()
{
    Q_ASSERT(m_isTransactionFinished);
    m_isTransactionFinished = false;
}


void CommandsWrapper::readStateFromHeader(void (CommandsWrapper::*signal)(void))
{
    const static QRegularExpression CURRENT_USER_LEVEL("\"current_user_level\">(\\d+)<");
    const static QRegularExpression CURRENT_USER_HEALTH("\"current_user_health\">(\\d+)<");
    const static QRegularExpression REMAINING_FIGHTS_COUNT("\"remaining_fights_count\">\\s+(\\d+)\\/(\\d+)");
    const static QRegularExpression CURRENT_GAME_TIME("\"current_game_time\">\\W+\\d+:(\\d+):(\\d+)");
    const static QRegularExpression USER_SILVER_AMOUNT(R"**("user_silver_amount">(\d+))**");
    const static QRegularExpression USER_GOLD_AMOUNT(R"**("user_gold_amount">(\d+))**");
    const static QRegularExpression USER_CRYSTAL_AMOUNT(R"**("user_crystal_amount">(\d+))**");
    const static QRegularExpression CURRENT_PET_STATE(R"**(<a href="\/game\/pet">\W+.+info_block-pet_(\w+))**");
    const static QRegularExpression CURRENT_PET_HEALTH(R"**("current_pet_health">(\d+))**");

    m_webView.page()->toHtml([=](const QString &html)
    {
        QRegularExpressionMatch match = CURRENT_USER_LEVEL.match(html);
        m_hero.setLevel(match.captured(1).toInt());

        match = CURRENT_USER_HEALTH.match(html);
        m_hero.setHealth(match.captured(1).toInt());

        match = USER_SILVER_AMOUNT.match(html);
        m_hero.setSilverAmount(match.captured(1).toInt());

        match = USER_GOLD_AMOUNT.match(html);
        m_hero.setGoldAmount(match.captured(1).toInt());

        match = USER_CRYSTAL_AMOUNT.match(html);
        m_hero.setCrystalAmount(match.captured(1).toInt());

        match = CURRENT_PET_STATE.match(html);
        if (!match.hasMatch())
        {
            m_hero.pet().setState(Pet::noPet);
        }
        else
        {
            QString state = match.captured(1);
            if (state == "active")
                m_hero.pet().setState(Pet::active);
            else if (state == "inactive")
                m_hero.pet().setState(Pet::inactive);
            else
                m_hero.pet().setState(Pet::dead);
            match = CURRENT_PET_HEALTH.match(html);
            m_hero.pet().setHealth(match.captured(1).toInt());
        }

        match = REMAINING_FIGHTS_COUNT.match(html);
        m_gameState.setFightsCount(match.captured(1).toInt());
        m_gameState.setMaxFightsCount(match.captured(2).toInt());

        match = CURRENT_GAME_TIME.match(html);
        m_gameState.setNextFightTime(QTime(0, match.captured(1).toInt(), match.captured(2).toInt()));

        m_gameState.setIsSomethingHappened(html.contains("Произошли новые события"));
        m_gameState.setGameDate(getCurrentDate(html));

        emit (this->*signal)();

    });
}

void CommandsWrapper::fillTrainCosts(const QString &html, Person & person)
{
    const QRegularExpression costsRe(R"**(Цена улучшения: (\d+))**");
    const QRegularExpression levelsRe(R"**(Уровень: (\d+))**");
    QRegularExpressionMatchIterator costsIter = costsRe.globalMatch(html);
    QRegularExpressionMatchIterator levelsIter = levelsRe.globalMatch(html);

    for (int i = Person::power; i <= Person::weight; ++i)
    {
        Person::FightFeatures feature = static_cast<Person::FightFeatures>(i);
        person.setNextFightFeatureCost(feature, costsIter.next().captured(1).toInt());
        person.setCurrentFightFeatureLevel(feature, levelsIter.next().captured(1).toInt());
    }
}

void CommandsWrapper::fillPatrolInfo(const QString &html)
{
    const QRegularExpression haveTimeRe(R"**(Осталось времени на поход сегодня: (\d+))**");
    const QRegularExpression inPatrolTimeRe(R"**(Ты сейчас в походе, осталось: <span .*>(\d+):(\d+):(\d+))**");

    const QRegularExpressionMatch match = inPatrolTimeRe.match(html);
    QTime inPatrolTime(0, 0, 0);
    if (match.hasMatch())
        inPatrolTime.setHMS(match.captured(1).toInt(), match.captured(2).toInt(), match.captured(3).toInt());
    m_gameState.setPatrolTime(inPatrolTime);

    m_gameState.setMaxPatrolTime(haveTimeRe.match(html).captured(1).toInt());
}

bool CommandsWrapper::isTransactionFinished() const
{
    return m_isTransactionFinished;
}

