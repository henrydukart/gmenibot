#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QTimer>

class BotManager;

namespace Ui {
class MainWindow;
}


class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

private slots:
    void startBot();
    void pauseBot();
    void timerTick();

public:
    virtual void closeEvent(QCloseEvent* event) override;

private:
    void loadSettings();
    void saveSettings();
    void connectGUISlots();
    void setGUIParams();

private:
    Ui::MainWindow *ui;

    QTimer m_minuteTimer;

    BotManager * m_botManager;
    QString m_settingsFile;
};

#endif // MAINWINDOW_H
