#ifndef IACTIONMANAGER_H
#define IACTIONMANAGER_H

#include <QObject>

class ActionManager : public QObject
{
    Q_OBJECT
public:
    explicit ActionManager(QObject *parent = nullptr)
        : QObject(parent)
    {

    }

    virtual void start() = 0;
    virtual bool canStart() const = 0;

signals:
    void info(QString);
    void finnished();
};

#endif // IACTIONMANAGER_H
