#ifndef INTERCEPTORWEBPAGE_H
#define INTERCEPTORWEBPAGE_H

#include <QWebEnginePage>

class InterceptorWebPage : public QWebEnginePage
{
    Q_OBJECT

public:
    using QWebEnginePage::QWebEnginePage;

protected:
    virtual bool acceptNavigationRequest(const QUrl & url, QWebEnginePage::NavigationType type, bool isMainFrame) override;
};

#endif // INTERCEPTORWEBPAGE_H
