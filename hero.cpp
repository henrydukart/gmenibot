#include "hero.h"

int Person::maxHealth() const
{
    return m_maxHealth;
}

void Person::setMaxHealth(int maxHealth)
{
    m_maxHealth = maxHealth;
}

int Person::health() const
{
    return m_health;
}

void Person::setHealth(int health)
{
    m_health = health;
}

int Person::nextFightFeatureCost(Person::FightFeatures feature) const
{
    return m_fightFeatureLevelAndCost[feature].second;
}

void Person::setNextFightFeatureCost(Person::FightFeatures feature, int cost)
{
    m_fightFeatureLevelAndCost[feature].second = cost;
}

int Person::currentFightFeatureLevel(Person::FightFeatures feature) const
{
    return m_fightFeatureLevelAndCost[feature].first;
}

void Person::setCurrentFightFeatureLevel(Person::FightFeatures feature, int level)
{
    m_fightFeatureLevelAndCost[feature].first = level;
}



int Hero::level() const
{
    return m_level;
}

void Hero::setLevel(int level)
{
    m_level = level;
}

int Hero::silverAmount() const
{
    return m_silverAmount;
}

void Hero::setSilverAmount(int silverAmount)
{
    m_silverAmount = silverAmount;
}

int Hero::crystalAmount() const
{
    return m_crystalAmount;
}

void Hero::setCrystalAmount(int crystalAmount)
{
    m_crystalAmount = crystalAmount;
}

int Hero::goldAmount() const
{
    return m_goldAmount;
}

void Hero::setGoldAmount(int goldAmount)
{
    m_goldAmount = goldAmount;
}

Pet& Hero::pet()
{
    return m_pet;
}

const Pet &Hero::pet() const
{
    return m_pet;
}

Pet::PetState Pet::state() const
{
    return m_state;
}

void Pet::setState(const PetState &state)
{
    m_state = state;
}
