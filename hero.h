#ifndef HERO_H
#define HERO_H

#include <QMap>

class Person
{
public:

    enum FightFeatures
    {
        power,
        protection,
        dexterity,
        skill,
        weight
    };

    int maxHealth() const;
    void setMaxHealth(int maxHealth);

    int health() const;
    void setHealth(int health);

    int nextFightFeatureCost(FightFeatures feature) const;
    void setNextFightFeatureCost(FightFeatures feature, int cost);

    int currentFightFeatureLevel(FightFeatures feature) const;
    void setCurrentFightFeatureLevel(FightFeatures feature, int level);

private:
    int m_maxHealth;
    int m_health;

    QMap<FightFeatures, QPair<int, int>> m_fightFeatureLevelAndCost;
};

class Pet: public Person
{
public:
    enum PetState
    {
        noPet,
        active,
        inactive,
        dead,
    };

    PetState state() const;
    void setState(const PetState &state);

private:
    PetState m_state;
};

class Hero : public Person
{
public:

    int level() const;
    void setLevel(int level);

    int silverAmount() const;
    void setSilverAmount(int silverAmount);

    int crystalAmount() const;
    void setCrystalAmount(int crystalAmount);

    int goldAmount() const;
    void setGoldAmount(int goldAmount);

    Pet& pet();
    const Pet& pet() const;

private:
    Pet m_pet;

    int m_level;

    int m_silverAmount;
    int m_crystalAmount;
    int m_goldAmount;
};

#endif // HERO_H
