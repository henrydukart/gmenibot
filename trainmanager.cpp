#include "trainmanager.h"
#include "commandswrapper.h"

#include <QDebug>

TrainManager::TrainManager(CommandsWrapper &commandsWrapper, const BotSettings &settings, QObject *parent)
    : ActionManager(parent)
    , m_commandsWrapper(commandsWrapper)
    , m_settings(settings)
{
    connect(&m_commandsWrapper, &CommandsWrapper::trained, this, &TrainManager::selectFeature);
}

void TrainManager::start()
{
    selectFeature();
}

bool TrainManager::canStart() const
{
    if (!m_settings.m_needTrainHero && !m_settings.m_needTrainPet)
        return false;

    const int silver = m_commandsWrapper.hero().silverAmount();

    auto check = [&](const Person &person, const QMap<Person::FightFeatures, int> & trainLevel)
    {
        for (auto it = trainLevel.cbegin(); it != trainLevel.cend(); ++it)
        {
            if (it.value() > person.currentFightFeatureLevel(it.key()) &&
                    silver >= person.nextFightFeatureCost(it.key()))
            {  
                return true;
            }
        }
        return false;
    };

    const Hero & hero = m_commandsWrapper.hero();
    if (m_settings.m_needTrainHero && check(m_commandsWrapper.hero(), m_settings.m_heroTrainParams))
        return true;

    if (hero.pet().state() != Pet::noPet && hero.pet().state() != Pet::dead && m_settings.m_needTrainPet)
        return check(m_commandsWrapper.hero().pet(), m_settings.m_petTrainParams);

    return false;
}

void TrainManager::selectFeature()
{
    if (!canStart())
    {
        emit finnished();
        return;
    }

    const int silver = m_commandsWrapper.hero().silverAmount();
    auto findMostExpansiveFeatureToTrain = [&](const Person &person, const QMap<Person::FightFeatures, int> & trainLevel)
    {
        int bestFeature = -1;
        int maxSilver = 0;

        for (auto it = trainLevel.cbegin(); it != trainLevel.cend(); ++it)
        {
            const int nextFeatureCost = person.nextFightFeatureCost(it.key());
            if (it.value() > person.currentFightFeatureLevel(it.key()) &&
                    silver >= nextFeatureCost &&
                    nextFeatureCost > maxSilver)
            {
                maxSilver = nextFeatureCost;
                bestFeature = it.key();
            }
        }

        return bestFeature;
    };

    int heroFeature = -1, petFeature = -1;
    if (m_settings.m_needTrainHero)
        heroFeature = findMostExpansiveFeatureToTrain(m_commandsWrapper.hero(), m_settings.m_heroTrainParams);
    if (m_settings.m_needTrainPet &&
            m_commandsWrapper.hero().pet().state() != Pet::noPet &&
            m_commandsWrapper.hero().pet().state() != Pet::dead)
        petFeature = findMostExpansiveFeatureToTrain(m_commandsWrapper.hero().pet(), m_settings.m_petTrainParams);

    if (heroFeature == -1 && petFeature == -1)
    {
        emit finnished();
        return;
    }

    if (petFeature == -1 || (heroFeature != -1 &&
            (m_commandsWrapper.hero().nextFightFeatureCost(static_cast<Person::FightFeatures>(heroFeature)) >=
                             m_commandsWrapper.hero().pet().nextFightFeatureCost(static_cast<Person::FightFeatures>(petFeature)))))
    {
        m_commandsWrapper.train(BotSettings::hero, static_cast<Person::FightFeatures>(heroFeature));
        emit info("Тренировка героя");
    }
    else
    {
        m_commandsWrapper.train(BotSettings::pet, static_cast<Person::FightFeatures>(petFeature));
        emit info("Тренировка питомца");
    }
}
