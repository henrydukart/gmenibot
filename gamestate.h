#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "hero.h"

#include <QMap>
#include <QVariant>
#include <QTime>

class GameState
{
public:
    GameState();

    int fightsCount() const;
    void setFightsCount(int fightsCount);

    int maxFightsCount() const;
    void setMaxFightsCount(int maxFightsCount);

    QTime nextFightTime() const;
    void setNextFightTime(const QTime &nextFightTime);

    bool isFullFights() const;

    bool isSomethingHappened() const;
    void setIsSomethingHappened(bool isSomethingHappened);

    bool isInPatrol() const;

    int maxPatrolTime() const;
    void setMaxPatrolTime(int maxPatrolTime);

    QTime patrolTime() const;
    void setPatrolTime(const QTime &patrolTime);

    QMap<Person::FightFeatures, int> enemyFightFeatures() const;
    void setEnemyFightFeatures(const QMap<Person::FightFeatures, int> &enemyFightFeatures);

    QDateTime gameDate() const;
    void setGameDate(const QDateTime &gameDate);

    QVariant m_potionCount;
    QVariant m_lastPotionDrink;

    bool m_isFriend;
    bool m_isMarked;

private:
    int m_fightsCount;
    int m_maxFightsCount;
    QTime m_nextFightTime;

    bool m_isSomethingHappened;

    QTime m_patrolTime;
    int m_maxPatrolTime;

    QMap<Person::FightFeatures, int> m_enemyFightFeatures;

    QDateTime m_gameDate;
};

#endif // GAMESTATE_H
