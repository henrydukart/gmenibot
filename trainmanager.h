#ifndef TRAINMANAGER_H
#define TRAINMANAGER_H

#include "actionmanager.h"

class CommandsWrapper;
struct BotSettings;

class TrainManager : public ActionManager
{
    Q_OBJECT
public:
    TrainManager(CommandsWrapper & commandsWrapper, const BotSettings & settings,QObject *parent = nullptr);
    virtual void start() override;
    virtual bool canStart() const override;

private slots:
    void selectFeature();

private:
    CommandsWrapper & m_commandsWrapper;
    const BotSettings & m_settings;
};

#endif // TRAINMANAGER_H
