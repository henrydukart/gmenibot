#ifndef FIGHTMANAGER_H
#define FIGHTMANAGER_H

#include "actionmanager.h"
#include <QVariant>

struct BotSettings;
class CommandsWrapper;
class Hero;
class GameState;


class FightManager : public ActionManager
{
    Q_OBJECT
public:
    FightManager(CommandsWrapper & wrapper, const BotSettings & settings, QObject *parent = nullptr);
    virtual void start() override;
    virtual bool canStart() const override;

private slots:
    void getPotionCount();
    void drinkPotion();
    void letOutPet();
    void selectEnemy();
    void fightWithEnemy();
    void afterFight();

private:
    void finish();
    bool canDrinkPotion() const;

private:
    CommandsWrapper & m_commandsWrapper;
    const BotSettings & m_botSettings;

    bool m_isBeforeFights;
};

#endif // FIGHTMANAGER_H
