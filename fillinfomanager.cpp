#include "fillinfomanager.h"
#include "commandswrapper.h"

FillInfoManager::FillInfoManager(CommandsWrapper & wrapper, QObject *parent)
    : ActionManager(parent)
    , m_commandsWrapper(wrapper)
{
    connect(&m_commandsWrapper, &CommandsWrapper::heroTrainInfoLoaded,
            this, &FillInfoManager::heroTrainInfoLoaded);
    connect(&m_commandsWrapper, &CommandsWrapper::petTrainInfoLoaded,
            this, &FillInfoManager::petTrainInfoLoaded);
    connect(&m_commandsWrapper, &CommandsWrapper::patrolInfoLoaded,
            this, &FillInfoManager::finnished);
}

void FillInfoManager::start()
{
    m_commandsWrapper.fillHeroTrainInfo();
    emit info("Загрузка параметров героя");
}

bool FillInfoManager::canStart() const
{
    return true;
}

void FillInfoManager::heroTrainInfoLoaded()
{
    if (m_commandsWrapper.hero().pet().state() != Pet::noPet)
    {
        m_commandsWrapper.fillPetTrainInfo();
        emit info("Загрузка параметров питомца");
        return;
    }

    petTrainInfoLoaded();
}

void FillInfoManager::petTrainInfoLoaded()
{
    m_commandsWrapper.fillPatrolInfo();
    emit info("Загрузка информации о походе");
}
